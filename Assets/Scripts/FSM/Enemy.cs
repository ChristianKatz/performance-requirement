﻿using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace UEGP3PR
{
    public class Enemy : MonoBehaviour
    {
        // the current state of the enemy
        private IEnemyState _currentState;
        // the player position to know the distance to the enemy
        public Transform PlayerPosition { get; set;}

        [Header("Movement")] 
        [Tooltip("the speed how fast the enemy should go back to the arena center")]
        [SerializeField] private float _goBackSpeed = 2;
        [Tooltip("the rotation speed of the enemy")] 
        [SerializeField] private float _rotationSpeed = 10;
        [Tooltip("enemy up and down speed")]
        [SerializeField] private float _hoveringSpeed = 2;
        [Tooltip("the lenght how long the enemy should go up and down")]
        [Range(0f, 5f)]
        [SerializeField] private float _hoveringLength = 1f;
        [Tooltip("time to wait for the next attack when the enemy reached the arena center")] 
        public float NextAttackCooldown;
        // the timer which counts down the attack stop
        public float NextAttackTimer {get; set;}
        
        [Header("Melee Attack")]
        [Tooltip("the hit range of the enemy")]
        [SerializeField] private float _meleeHitRange = 1;
        [Tooltip("speed how fast the enemy reached the player to execute the melee attack")]
        [SerializeField] private float _movementAttackSpeed = 2;
        // decided when the enemy was hit
        public bool MeleeHit { get; set;} = false;
        
        [Header("Spread Missile Ability")]
        [Tooltip("the middle of the arena is the point where the corresponding ability will be fired")]
        [SerializeField] private Transform _arenaCenterPosition;
        [Tooltip("the missile prefab")] 
        [SerializeField] private GameObject _missilePrefab;
        [Tooltip("missile spawn position")] 
        [SerializeField] private Transform[] _missileSpawnPositions;
        
        [Header("Shield Ability")]
        [Tooltip("the shield where the enemy is invulnerable for a short time")]
        public GameObject Shield;
        // the shield using time
        public float ShieldUsingTime { get; set;}
        [Tooltip("the time how long the shield can be used")]
        public float ShieldTime = 3;
        [Tooltip("the time how long the enemy can't use his shield")]
        public float ShieldCooldown = 20;
        // the cooldown to use the shield again
        public float ReuseShieldCooldown { get; set;}
        
        [Header("Meteorite Fall Ability")]
        [SerializeField] private GameObject _meteoritePrefab;
        [Tooltip("the spawn positions of the meteorites")]
        [SerializeField] private Transform[] _meteoriteSpawnPosition;
        // the cooldown to use the meteorite ability again
        public float MeteoriteReuseTime { get; set; }
        [Tooltip("the time how long this ability can't be used")]
        public float MeteoriteCooldown = 10;
        
        [Header("Range Bullet Attack")]
        [Tooltip("the bullet prefab")] 
        [SerializeField] private GameObject _bulletPrefab;
        [Tooltip("the shoot position where the bullet will spawn")]
        [SerializeField] private GameObject _shootPosition;

        [Header("Change Position Ability")]
        [Tooltip("the waypoints to change position")]
        [SerializeField] public Transform[] WayPoints;
        [Tooltip("the speed how fast the enemy should change the position")]
        [SerializeField] private float _changePositionSpeed = 6;
        // choose the next transform for the enemy in a array
        public int PositionChanger { get; set; }
        
        public static IdleState IdleState = new IdleState();
        public static MeleeAttackState MeleeAttackState = new MeleeAttackState();
        public static SpreadMissileState SpreadMissileState = new SpreadMissileState();
        public static DefenseState DefenseState = new DefenseState();
        public static ChangePositionState ChangePositionState = new ChangePositionState();
        public static RangeAttackState RangeAttackState = new RangeAttackState();
        public static MeteoriteFallState MeteoriteFallState = new MeteoriteFallState();
        
        void Start()
        {
            // the first state of the enemy will be the idle state
            _currentState = IdleState;
            
            // get the position of the player
            PlayerPosition = FindObjectOfType<FPSController>().transform;

            // set the shield cooldown
            ReuseShieldCooldown = ShieldCooldown;
            // set the shield using time
            ShieldUsingTime = ShieldTime;
            // set the meteorite cooldown
            MeteoriteReuseTime = MeteoriteCooldown;
            // set the next attack cooldwon
            NextAttackTimer = NextAttackCooldown;
        }

        void Update()
        {
            //state machine that will handle the different states
            IEnemyState nextState = _currentState.Execute(this);
            if (nextState != _currentState)
            {
                _currentState.Exit(this);
                _currentState = nextState;
                _currentState.Enter(this);
            }
            
            // the shield will be available every few seconds, otherwise it could be too strong
            ReuseShieldCooldown -= Time.deltaTime;
            // the ability will be available every few seconds, otherwise it could be too strong
            MeteoriteReuseTime -= Time.deltaTime;
            // the enemy always shows in the direction of the player to attack him precisely
            transform.rotation = Quaternion.Slerp(transform.rotation, 
                Quaternion.LookRotation(PlayerPosition.position - transform.position), Time.deltaTime * _rotationSpeed);
        }
        
        /// <summary>
        /// the enemy goes back to the arena center
        /// </summary>
        public void BackToTheArenaCenter()
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(_arenaCenterPosition.position.x,
                5, _arenaCenterPosition.position.z), Time.deltaTime * _goBackSpeed);
        }
        
        /// <summary>
        /// calculates the distance between the arena center and the enemy
        /// </summary>
        /// <returns></returns>
        public float DistanceToArenaCenter()
        {
            return Vector3.Distance(transform.position, new Vector3(_arenaCenterPosition.position.x, 5, _arenaCenterPosition.position.z));
        }
        
        /// <summary>
        /// The distance between the enemy and the player
        /// </summary>
        /// <returns></returns>
        public float DistanceToPlayer()
        {
            return Vector3.Distance(PlayerPosition.position, transform.position);
        }
        
        /// <summary>
        /// moves the character up and down
        /// </summary>
        public void EnemyHovering()
        {
            transform.position += new Vector3(0,Mathf.Sin(Time.time * _hoveringSpeed) * _hoveringLength / 100, 0);
        }
        
        /// <summary>
        /// enemy goes in hit range to cause damage
        /// </summary>
        public void MeleeAttack()
        {
            // moves to the player until it reaches the hit range
            if (DistanceToPlayer() > _meleeHitRange)
            {
                Vector3 _moveToPlayer = Vector3.Lerp(transform.position, PlayerPosition.position, Time.deltaTime * _movementAttackSpeed);
                transform.position = _moveToPlayer;

            }
            // hits the character
            else
            {
                MeleeHit = true;
            }
        }
        
        /// <summary>
        /// enemy goes in defense mode for a fixed time
        /// </summary>
        public void Defense()
        {
            // using time of the shield
            ShieldUsingTime -= Time.deltaTime;
            // activates the shield
            Shield.SetActive(true);
        }

        /// <summary>
        /// change the position of the enemy between fixed points
        /// </summary>
        public void ChangePosition()
        {
            // goes to the next position
            Vector3 destination = Vector3.Lerp(transform.position, WayPoints[PositionChanger].position, Time.deltaTime * _changePositionSpeed);
            transform.position = destination;
        }
        
        /// <summary>
        /// the distance between the next position in the waypoints and the current position of the enemy
        /// </summary>
        /// <returns></returns>
        public float DistanceToNextPoint()
        {
            return Vector3.Distance(transform.position, WayPoints[PositionChanger].position);
        }
        
        /// <summary>
        /// spawns the bullets for the range attack after the position change
        /// </summary>
        public void SpawnBullet()
        {
            Instantiate(_bulletPrefab, _shootPosition.transform.position, Quaternion.identity);
        }
        
        /// <summary>
        /// spawns the meteorites
        /// </summary>
        public void MeteoriteSpawn()
        {
            for (int i = 0; i < _meteoriteSpawnPosition.Length; i++)
            {
                Instantiate(_meteoritePrefab, _meteoriteSpawnPosition[i].position, Quaternion.identity);
            }
        }
        
        /// <summary>
        /// spawn the missiles
        /// </summary>
        public void SpawnMissiles()
        {
            for (int i = 0; i < _missileSpawnPositions.Length; i++)
            {
                Instantiate(_missilePrefab, _missileSpawnPositions[i].position, Quaternion.Euler(-90,0,0));
            }
        }
    }
}
