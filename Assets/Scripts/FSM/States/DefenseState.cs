﻿namespace UEGP3PR
{
    public class DefenseState : IEnemyState
    {
        public IEnemyState Execute(Enemy enemy)
        {
            // as long as the shield using time is over 0, the enemy is protected from damage
            // the enemy is hovering like in the Idle state
            if (enemy.ShieldUsingTime > 0)
            {
                enemy.Defense();
                enemy.EnemyHovering();
                return Enemy.DefenseState;
            }
            // when the time is up, the shield will be activated and goes back to Idle
            else
            {
                enemy.Shield.SetActive(false);
                return Enemy.IdleState;
            }
        }

        public void Enter(Enemy enemy)
        {

        }

        public void Exit(Enemy enemy)
        {
            // reset the using time to be able to use the shield again
            enemy.ShieldUsingTime = enemy.ShieldTime;
            // the value will be reset to 20 seconds to reuse the ability
            enemy.ReuseShieldCooldown = enemy.ShieldCooldown;
        }
    }
}

