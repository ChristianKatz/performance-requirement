﻿namespace UEGP3PR
{
    public class MeteoriteFallState : IEnemyState
    {
        public IEnemyState Execute(Enemy enemy)
        {
            // instantly goes back to Idle because the meteorites will spawn at the enter method
            return Enemy.IdleState;
        }

        public void Enter(Enemy enemy)
        {
            // spawn the meteorites
            enemy.MeteoriteSpawn();
        }

        public void Exit(Enemy enemy)
        {
        
        }
    }
}

