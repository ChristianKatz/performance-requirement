﻿namespace UEGP3PR
{
    public class RangeAttackState : IEnemyState
    {
        public IEnemyState Execute(Enemy enemy)
        {
            // instantly goes back to the Idle state, because the bullet spawn happens in the exit state
            return Enemy.IdleState;
        }

        public void Enter(Enemy enemy)
        {
        
        }

        public void Exit(Enemy enemy)
        {
            // spawn the bullets to shoot them
            enemy.SpawnBullet();
        }
    }
}

