﻿using UnityEngine;

namespace UEGP3PR
{
    public class IdleState : IEnemyState
    {
        public IEnemyState Execute(Enemy enemy)
        {
            // the enemy is hovering up and down
            enemy.EnemyHovering();
            // every time an ability is finished the player goes back to the center of the arena
            enemy.BackToTheArenaCenter();
            
            // when the player reached the arena center the next attack timer is running
            if (enemy.DistanceToArenaCenter() < 1f)
            {
                enemy.NextAttackTimer -= Time.deltaTime;
                
                // when the next attack timer is 0 the next ability will be chosen
                if (enemy.NextAttackTimer <= 0)
                {
                    // set back the value
                    enemy.NextAttackTimer = enemy.NextAttackCooldown;
                    // after the cooldown, the enemy will use his shield to protect himself
                    if (enemy.ReuseShieldCooldown <= 0)
                    {
                        return Enemy.DefenseState;
                    }
                
                    // take a random value to chose the next ability
                    float nextAttack = Random.Range(0, 4);
                    // switch between different abilities
                    switch (nextAttack)
                    {
                        case 0:
                            return Enemy.MeleeAttackState;
                        case 1:
                            return Enemy.SpreadMissileState;
                        case 2:
                            return Enemy.ChangePositionState;
                        case 3:
                            // if the time is up, it is possible to use the ability again
                            // otherwise, the next ability will be chosen
                            if (enemy.MeteoriteReuseTime < 0)
                            {
                                // set back the value to wait for the next using of the attack
                                enemy.MeteoriteReuseTime = enemy.MeteoriteCooldown;
                                return Enemy.MeteoriteFallState;
                            }
                            else
                            {
                                return Enemy.IdleState;
                            }
                    }
                }
            }
            return Enemy.IdleState;
        }
    
        public void Enter(Enemy enemy)
        {

        }
    
        public void Exit(Enemy enemy)
        {

        }
    }
}

