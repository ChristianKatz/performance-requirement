﻿namespace UEGP3PR
{
    public class MeleeAttackState : IEnemyState
    {
        public IEnemyState Execute(Enemy enemy)
        {
            // when the enemy has not executed the melee hit already, he will move towards the player and hit him
            if (!enemy.MeleeHit)
            {
                enemy.MeleeAttack();
                return Enemy.MeleeAttackState;
            }
            else
            {
                return Enemy.IdleState;
            }
        }

        public void Enter(Enemy enemy)
        {
            
        }

        public void Exit(Enemy enemy)
        {
            // reset the value for the next melee attack
            enemy.MeleeHit = false;
        }
    }
}
