﻿namespace UEGP3PR
{
    public class SpreadMissileState : IEnemyState
    {
        public IEnemyState Execute(Enemy enemy)
        {
            // instantly goes back to the Idle state, because the missile spawns happen in the exit state
            return Enemy.IdleState;
        }

        public void Enter(Enemy enemy)
        {
        
        }

        public void Exit(Enemy enemy)
        {
            // the spawn of the missiles to fire them on the enemy
            enemy.SpawnMissiles();
        }
    }
}
