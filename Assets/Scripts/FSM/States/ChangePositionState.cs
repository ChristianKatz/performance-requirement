﻿namespace UEGP3PR
{
    public class ChangePositionState : IEnemyState
    {
        public IEnemyState Execute(Enemy enemy)
        {
            // when the enemy hasn't reached the next position, he will move towards it
            // otherwise the range attack state will be executed
            if (enemy.DistanceToNextPoint() > 0.2f)
            {
                enemy.ChangePosition();
                return Enemy.ChangePositionState;
            }
            else
            {
                return Enemy.RangeAttackState;
            }

        }

        public void Enter(Enemy enemy)
        {
        
        }

        public void Exit(Enemy enemy)
        {
            // set the position value +1 to change the transform in the array
            enemy.PositionChanger++;
            // when the position value is over the ways points length, it will be set back to 0
            enemy.PositionChanger %= enemy.WayPoints.Length;
        }
    }
}

