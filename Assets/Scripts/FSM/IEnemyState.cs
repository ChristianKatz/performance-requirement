﻿namespace UEGP3PR
{
	public interface IEnemyState
	{
		// the Execute method is for the update of the state
		IEnemyState Execute(Enemy enemy);
		// the Enter method will be executed once when the state will be entered
		void Enter(Enemy enemy);
		// the Exit method will be executed once when the state will be exited
		void Exit(Enemy enemy);
	}
	
}

