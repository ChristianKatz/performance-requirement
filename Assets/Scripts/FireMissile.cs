﻿using UnityEngine;

namespace UEGP3PR
{
    public class FireMissile : MonoBehaviour
    {
        // the cooldown when the missiles aiming towards the player
        private float _towardsPlayerCooldown = 0.5f;

        // the script of the enemy to get the player position
        private Enemy _enemy;
    
        private void Awake()
        {
            // get the component
            _enemy = FindObjectOfType<Enemy>();
        }
    
        void Update()
        {
            ShootMissile();
        }

        /// <summary>
        /// shoots the missile in the air and finally towards the enemy
        /// </summary>
        private void ShootMissile()
        {
            // shoots the missile along the z axis
            transform.position += transform.forward * Time.deltaTime * 8;

            // counts down the time to change aiming
            _towardsPlayerCooldown -= Time.deltaTime;

            // when the time is at 0, the missiles rotate towards the player
            if (_towardsPlayerCooldown <= 0)
            {
                Quaternion rotatesTowardsPlayer = Quaternion.Slerp(transform.rotation, 
                    Quaternion.LookRotation(_enemy.PlayerPosition.position - transform.position), Time.deltaTime * 5);
                transform.rotation = rotatesTowardsPlayer;
            }

            // when the missiles have reached the player, they will be destroyed
            if (Vector3.Distance(transform.position, _enemy.PlayerPosition.position) < 2)
            {
                Destroy(transform.gameObject);
            }
        }
    }

}
