﻿using UnityEngine;
using UnityEngine.AI;

namespace UEGP3PR
{
    public class RollingMeteorite : MonoBehaviour
    {
        // the nav mesh agent that controls the meteorites
        private NavMeshAgent _navMeshAgent;
        // the enemy script to get the player position
        private Enemy _enemy;

        void Start()
        {
            // get the components
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _enemy = FindObjectOfType<Enemy>();
        }

        void Update()
        {
            RollingTowardsPlayer();
        }

        /// <summary>
        /// meteorite rolls towards the player to cause damage
        /// </summary>
        private void RollingTowardsPlayer()
        {
            // updates the destination of the player
            _navMeshAgent.SetDestination(_enemy.PlayerPosition.position);

            // when the meteorites reaches the player, they will be destroyed
            if (Vector3.Distance(transform.position, _enemy.PlayerPosition.position) < 2)
            {
                Destroy(gameObject);
            }
        }
    }
}
