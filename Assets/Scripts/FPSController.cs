﻿using UnityEngine;

namespace UEGP3PR
{
    public class FPSController : MonoBehaviour
    {
        // the current movement speed of the player
        private Vector3 _currentForwardSpeed;
        // the reference of the current velocity of the player
        private Vector3 _refVelocity;

        [Header("Movement")]
        [Tooltip("the movement speed of the player")]
        [SerializeField] private float _movementSpeed;
        [Tooltip("smooth the rotation of the player")] 
        [SerializeField] private float _smoothRotation;
        [Tooltip("smooth the movement speed of the player")] 
        [SerializeField] private float _smoothMovementSpeed;
        [Tooltip("the rotation speed of the player")]
        [SerializeField] private float _rotationSpeed;
        
        [Header("Gravity")]
        [Tooltip("the gravity force of the player")]
        [SerializeField] private float _gravityForce;
        
        [Header("Ground Check")]
        [Tooltip("the radius to check if the player is grounded")]
        [SerializeField] private float _groundCheckRadius;
        [Tooltip("the layer mask for the ground check")]
        [SerializeField] private LayerMask _layerMask;
        [Tooltip("the ground check Position")]
        [SerializeField] private Transform _groundCheckPosition;
        
        [Header("Player Camera")]
        [Tooltip("the speed, how fast the camera should rotate")]
        [SerializeField] private float _cameraRotationSpeed;
        [Tooltip("the transform for the player camera")]
        [SerializeField] private Transform _cameraPosition;
        
        // the direction where the player is looking
        private Vector3 _lookDirection;
        
        // the gravity of the player
        private float _gravity;

        // the character controller of unity
        private CharacterController _characterController;
        
        void Start()
        {
            // get the component
            _characterController = GetComponent<CharacterController>();
        }
        
        private void Update()
        {
            // the horizontal axis to move the character along the x axis
            float horizontalAxis = Input.GetAxis("Horizontal");
            // the vertical axis to move the character along the z axis
            float verticalAxis = Input.GetAxis("Vertical");
            
            // the mode direction of the player based on the current camera direction
            Vector3 moveDirection = Camera.main.transform.forward * verticalAxis + Camera.main.transform.right * horizontalAxis;
            
            // the character should not move into the ground or in the air so that it is set to 0
            moveDirection.y = 0;

            // the calculation of the current movement speed
            _currentForwardSpeed = Vector3.SmoothDamp(_currentForwardSpeed, moveDirection * _movementSpeed, ref _refVelocity, Time.deltaTime * _smoothMovementSpeed);

            // the rotation of the player
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0,_lookDirection.y,0) * _rotationSpeed),  Time.deltaTime * _smoothRotation);
            
            // the calculation of the gravity
            _gravity += Physics.gravity.y * Time.deltaTime * _gravityForce;
            
            // if the player is grounded, there is 0 gravity to not run it in the infinity
            if (Grounded())
            {
                _gravity = 0;
            }
            
            // move the character
            _characterController.Move(_currentForwardSpeed * Time.deltaTime + new Vector3(0,_gravity,0) * Time.deltaTime);
        }

        private void LateUpdate()
        {
            // the current position of the camera
            Camera.main.transform.position = _cameraPosition.position;
        
            // the mouse y and x axis to move the mouse
            float horizontalAxis = Input.GetAxis("Mouse X");
            float verticalAxis = Input.GetAxis("Mouse Y");
        
            // the calculation where the camera is looking
            _lookDirection += new Vector3(-verticalAxis,horizontalAxis, 0);

            // rotate the camera from the current position towards the desired direction
            Camera.main.transform.rotation = Quaternion.Slerp(Camera.main.transform.rotation, Quaternion.Euler(_lookDirection),
                Time.deltaTime * _cameraRotationSpeed);
        }
        
        /// <summary>
        /// checks if the character is grounded or not
        /// </summary>
        /// <returns></returns>
        private bool Grounded()
        {
            return Physics.CheckSphere(_groundCheckPosition.position, _groundCheckRadius, _layerMask);
        }
        
        /// <summary>
        /// shows the ground check
        /// </summary>
        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(_groundCheckPosition.position, _groundCheckRadius);
        }
    }
}

