﻿using UnityEngine;

namespace UEGP3PR
{
    public class FireBullet : MonoBehaviour
    {
        // the rigidbody of the bullet to fire it
        private Rigidbody _bulletRigidbody;
        // the gun to get the fire position
        private GameObject _gun;
        
        private void Start()
        {
            // get the component of the object
            _bulletRigidbody = GetComponent<Rigidbody>();
            _gun = GameObject.Find("Gun"); // I know, the find methods are expensive and hard coded stuff like this is not optimal because of the simple name changing ;)

            ShootBullet();
        }

        /// <summary>
        /// shoots the bullet
        /// </summary>
        private void ShootBullet()
        {
            _bulletRigidbody.AddForce(_gun.transform.forward * 50, ForceMode.Impulse);
        }
    
    }
}

